resource "gitlab_group" "infinite_omicron" {
  name      = "Infinite Omicron"
  parent_id = 0
  path      = "infinite-omicron"
}

resource "gitlab_group" "eng" {
  name      = "Engineering"
  parent_id = 6996391
  path      = "eng"
}

resource "gitlab_group" "terraform_flux_v2_gitlab" {
  name      = "terraform-flux-v2-gitlab"
  parent_id = 6997867
  path      = "terraform-flux-v2-gitlab"
}

resource "gitlab_project" "config" {
  name                   = "config"
  visibility_level       = "private"
  initialize_with_readme = false
  default_branch         = var.branch
  path                   = "config"
  namespace_id           = 54584146
}

resource "gitlab_deploy_key" "terraform-flux-v2-gitlab" {
  title   = "terraform-flux-v2-gitlab"
  project = gitlab_project.config.id
  key     = tls_private_key.main.public_key_openssh

  depends_on = [gitlab_project.config]
}

resource "gitlab_repository_file" "install" {
  project        = gitlab_project.config.id
  branch         = gitlab_project.config.default_branch
  file_path      = data.flux_install.main.path
  content        = base64encode(data.flux_install.main.content)
  commit_message = "Add ${data.flux_install.main.path}"

  depends_on = [gitlab_project.config]
}

resource "gitlab_repository_file" "sync" {
  project        = gitlab_project.config.id
  branch         = gitlab_project.config.default_branch
  file_path      = data.flux_sync.main.path
  content        = base64encode(data.flux_sync.main.content)
  commit_message = "Add ${data.flux_sync.main.path}"

  depends_on = [gitlab_repository_file.install]
}

resource "gitlab_repository_file" "kustomize" {
  project        = gitlab_project.config.id
  branch         = gitlab_project.config.default_branch
  file_path      = data.flux_sync.main.kustomize_path
  content        = base64encode(data.flux_sync.main.kustomize_content)
  commit_message = "Add ${data.flux_sync.main.kustomize_path}"

  depends_on = [gitlab_repository_file.sync]
}

resource "gitlab_repository_file" "patches" {
  for_each = data.flux_sync.main.patch_file_paths

  project        = gitlab_project.config.id
  file_path      = each.value
  content        = yamlencode(local.patches[each.key])
  branch         = var.branch
  commit_message = "Add ${each.key}"
}

