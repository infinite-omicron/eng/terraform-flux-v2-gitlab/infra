variable "gitlab_owner" {
  type = string
}

variable "gitlab_token" {
  type      = string
  sensitive = true
}

variable "target_path" {
  type    = string
  default = "local"
}

variable "branch" {
  type    = string
  default = "main"
}

