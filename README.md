# terraform-flux-v2-gitlab-infra

```mermaid
graph LR
  git -. commit .-> infra
  git -. commit .-> config

  terraform -. apply .-> config
  terraform -. apply .-> flux-v2

  config -. synchronization .-> flux-v2

  flux-v2 -. reconciliation .-> config

  subgraph operator
    git
    terraform
  end

  subgraph gitlab
    infra
    config
  end

  subgraph kubernetes
    flux-v2
  end
```

Using terraform to bootstrap flux v2 using a gitlab repository.

| Environment Variables |
| --------------------- |
| `TF_VAR_gitlab_owner` |
| `TF_VAR_gitlab_token` |

## Reference

- https://registry.terraform.io/providers/fluxcd/flux/latest/docs/guides/customize-flux
- https://registry.terraform.io/providers/fluxcd/flux/latest/docs/guides/gitlab
- https://github.com/fluxcd/terraform-provider-flux/pull/311

